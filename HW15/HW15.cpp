#include <iostream>
#include "HW15.h"

static int N = 15;

int main()
{
	PrintNumbers(N, true);
}

void PrintNumbers(int maxNumber, bool even) 
{
	for (int i = 0; i <= maxNumber; i++)
	{
		if ((even && i % 2 == 0) ||
			(!even && i % 2 != 0))
		{
			std::cout << i << "\n";
		}
	}
}